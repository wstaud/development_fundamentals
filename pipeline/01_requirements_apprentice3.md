# III. Programming / Analysis Requirements (Apprentice)

## Apprentice (3) Description
A competent junior programmer in C. Able to accomplish most C tasks at the competent but needs a spot check upon completion level. Will require some additional help in Python and Assembly. The equivalent of a company intern or first level engineer. This is a graduate of the IDF (Intro to Dev Fundamentals) course.


|Task| Sub-task & Description | Apprentice Proficiency Code |
|----|---------------------|-------------------|
| 1. | C Programming - Developers will demonstrate their understanding & coding capability in the use of the C programming language  | | | 
| | a. Developers will demonstrate basic, strong and formated Input/Output functions in their code development | 3c
| | b. Developers will demonstrate thorough understanding of Data types and Variables and be able to use them in their code development for C Programming | 3c |
| | c. Developers will demonstrate thorough understanding of C programming Operators and Expressions and will use operators within their code development | 3c |
| | d. Developers will describe Control Flow concepts and utilize coding style guide format, stub codes, statements and blocks, conditional statements, loops as well as nested control flow in code development | 3c
| | e. Developers will define and use functions as part of code development | 3c |
| | f. Developers will demonstrate an understanding of the C Preprocessor and correctly use the editors in code development | 2b |
| | g. Developers will apply basic knowledge of pointers and arrays, through use/manipulation in code development | 3c |
| | h. Developers will demonstrate a thorough understanding of advanced pointers and use them as part of their code development | 3c |
| | i. Developers will demonstrate the use of structures/unions/enumerators while coding in C | 3c |
| | j. Developers will have a strong understanding of memory management concepts, and apply these concepts in the coding techniques | 3c |
| | k. Developers will apply their knowledge of stack based memory and heap based memory, and be able to interact with memory management | 3c<sup>1</sup> |
| | l. Developers will apply a thorough understanding of header files in code development | 3c |
| | m. Developers will be able to cast between multiple data types in C | 3c |
| | n. Developers will understand the built in functions associated with the C standard library and will be able to use it in their coding | 3c |
| | o. Developers will apply an understanding through coding techniques of basic data structures: linked lists, queues, stack, trees | 3c<sup>2</sup> |
| | p. Developers will apply algorithms to the basic structures: linked lists, queues, stacks, trees | 2c<sup>3</sup> |
| | q. Developers will understand running time complexity and demonstrate calculating best conceivable run-time | 3c<sup>3</sup> |
| | r. Developers will understand defensive programming techniques and error handling paradigms | 3c<sup></sup> |
| | s. Developers will describe coding etiquette best practices, such as reasonable variable names and adequate spacing | 3c |
| | | |
| 2. | Python - Developers will demonstrate their understanding & coding capability in the use of Python programming | |
| | a. Developers will properly use data types variables as part of code development | 2c |
| | b. Developers will apply knowledge of coding etiquette through the proper use of whitespace/formatting and comments | 3c<sup>4</sup> |
| | c. Developers will understand the use of numeric data types and will be able to properly implement them | 2c |
| | d. Developers will understand Arithmetic Operations and be able to properly implement them | 2c | 
| | e. Developers will create/use strings and be able to properly implement them | 2c |
| | f. Developers will apply knowledge of Control Flow in coding techniques | 3c<sup>5</sup> |
| | g. Developers will implement sequence types in code development (str, unicode, list, tuple, bytearray, buffer, xrange) | 2c |
| | h. Developers will properly implement set types in code development | 2c |
| | i. Developers will apply knowledge of Python mapping types in code development | 2c |
| | j. Developers will implement Python comparisons in code development | 2c |
| | k. Developers will use conditional statements in code development | 2c |
| | m. Developers will apply knowledge of Generators, Iterators, Lambdas in code development | 1a<sup>6</sup> |
| | n. Developers will apply knowledge of modules to properly create and import them | 1a<sup>6</sup> | 
| | o. Developers will apply knowledge of loops in coding techniques | 3c<sup>5</sup> |
| | p. Developers will briefly describe object oriented programming as it relates to Python, and use this knowledge during code development | 1a<sup>6</sup> |
| | q. Developers will briefly describe the Python Standard Library (struct, re specifically) and use this knowledge during code development | 1a<sup>6</sup> |
| | r. Using reference material, developers will relate the differences between Python versions | B |
| | s. Developers will use Python in the Command Line/Terminal | 1a |
| | w. Developers will use Python to interact with native file systems (e.g., open, read, write, close) | 2c |
| | | |
| 3. | Networks Concepts - Developers will gain understanding of Network configurations and Networking fundamentals. They will apply their knowledge of Network concepts to perform socket programming.<sup>7</sup> |
| | a. Developers will describe the different layers of the OSI and TCP/IP Models | B |
| | b. Developers will understand IPv4 addressing | B |
| | c. Developers will be able to understand CIDR notation | C | 
| | d. Developers will have a basic understanding of ports and protocols | B |
| | e. Developers will be able to implement a TCP client using BSD sockets to communicate with a given server | 3c |
| | f. Developers will be able to implement a UDP client using BSD sockets to communicate with a given server | 3c |
| | g. Developers will be able to implement a TCP server using BSD sockets to communicate with their implemented client | 3c |
| | h. Developers will be able to implement a UDP client using BSD sockets to communicate with their implemented client | 3c |
| | i. Developers will be able to implement an HTTP client using an HTTP library | 3c |
| | | |
| 4. | Assembly - Developers will demonstrate their understanding & coding capability in the use of Assembly programming<sup>8</sup> |
| | a. Developers will know the differences between x86 and x64 architectures, including available registers and instructions | A |
| | b. Developers will recognize assembly operating system calls using call and sysenter | A |
| | c. Developers will recognize the stack frame, along with the assembly calling conventions to set it up in both x86 and x64  | A |
| | d. Developers will recognize the push/pop instructions and know what they do | B |
| | e. Developers will recognize the mov and lea instructions and know what they do | B |
| | f. Developers will recognize common assembly paradigms like xor eax, eax or test eax, eax and know what they represent | B |
| | g. Developers will understand branching instructions and understand the different conditions that influence the jump | B |
| | h. Developers will understand bitwise instructions, i.e., AND, OR, XOR | A | 
| | i. Developers will be able to understand memory dereferences versus immediate operations in Assembly | B |
| | j. Developers will be able to write memcpy in NASM while utilizing loops | 2c |
| | | |


# V. Supplemental Technical Skills<sup>9</sup>

|Task| Sub-task & Description | Apprentice Proficiency Code |
|----|---------------------|-------------------|
| 1. | GDB - Developers will demonstrate their ability to debug their programs using GDB | |
| | a. Developers will be able to set, unset, disable, enable and keep track of breakpoints using GDB | 3c |
| | b. Developers will be able to control execution flow using GDB | 3c |
| | c. Developers will demonstrate the ability to set conditional breakpoints in GDB | 3c |
| | d. Developers will demonstrate the ability to set watchpoints | 3c |
| | e. Developers will be able to inspect and set variables from within GDB | 3c |
| | | |
| 2. | GCC - Developers will demonstrate their ability to build their programs using GCC | |
| | a. Developers will be able to compile a single file .c file to a binary using GCC | 3c |
| | b. Developers will demonstrate the ability to create object files to link against using GCC | 3c | 
| 3. | Git - Developers will demonstrate their understanding and ability to use a Version Control System ||
| | a. Developers will have an understanding of the general purpose and capabilities of Git | B |
| | b. Developers will be able to create and work in a separate branch from the main development branch in their local environment | 3c |
| | c. Developers will be able to merge changes from the main development branch prior to committing changes | 3c |
| | d. Developers will be able to resolve merge conflicts | 3c |
| | | | 



## Changelog
| # | Description                                         |
|----|----------------------------------------------------|
| 1. | Updated task 1k. (stack/heap memory) from 2b to 3c |
| 2. | Updated task 1p. (data structures) from 2b to 3c   |
| 3. | Added additional tasks                             |
| N/A | Removed C++ section, removed Python meta-classes, multi-threading & ctypes. Removed Reverse Engineering.                  |
| 4. | Updated task 2b. (code etiquette) from 2b to 3c    |
| 5. | Updated task 2f. (control flow) & 2o. (loops) from 2b to 3c. Should be satisfied in course of the C material      |
| 6. | Downgraded task from 2c to 1a |
| 7. | Adjusted Networking portion to Winsock from Python and pared down material |
| 8. | Adjusted Assembly portion from task based to knowledge based
| 9. | Built out supplemental technical skills portion with GDB, GCC and Git |
| | | 


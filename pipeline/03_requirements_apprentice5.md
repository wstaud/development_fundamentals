# III. Programming / Analysis Requirements (Apprentice)

## Apprentice (5) Description
An experienced, competent junior programmer in C. Able to accomplish most C/Python tasks at the competent but needs a spot check upon completion level. The equivalent of an experienced first level engineer. The expectation is to be signed off for this level after approximately 500-1000 hours of development (**TODO:** approximately half-year of full-time development). The below are the changes from [Apprentice-3](01_requirements_apprentice3.md) level


|Task| Sub-task & Description | Apprentice Proficiency Code |
|----|---------------------|-------------------|
| 1. | C Programming - Developers will demonstrate their understanding & coding capability in the use of the C programming language  | | | 
| | t. Developers will recall basic concepts on stack pointers and how they are used in the stack| B |
| | | |
| 2. | Python - Developers will demonstrate their understanding & coding capability in the use of Python programming | |
| | a. Developers will properly use data types variables as part of code development | 3c |
| | b. Developers will apply knowledge of coding etiquette through the proper use of whitespace/formatting and comments | 3c |
| | c. Developers will understand the use of numeric data types and will be able to properly implement them | 3c |
| | d. Developers will understand Arithmetic Operations and be able to properly implement them | 3c | 
| | e. Developers will create/use strings and be able to properly implement them | 3c |
| | f. Developers will apply knowledge of Control Flow in coding techniques | 3c |
| | g. Developers will implement sequence types in code development (str, unicode, list, tuple, bytearray, buffer, xrange) | 3c |
| | h. Developers will properly implement set types in code development | 3c |
| | i. Developers will apply knowledge of Python mapping types in code development | 3c |
| | j. Developers will implement Python comparisons in code development | 3c |
| | k. Developers will use conditional statements in code development | 3c |
| | m. Developers will apply knowledge of Generators, Iterators, Lambdas in code development | 3c |
| | n. Developers will apply knowledge of modules to properly create and import them | 3c | 
| | o. Developers will apply knowledge of loops in coding techniques | 3c |
| | p. Developers will briefly describe object oriented programming as it relates to Python, and use this knowledge during code development | 3c |
| | q. Developers will briefly describe the Python Standard Library (struct, re specifically) and use this knowledge during code development | 3c |
| | r. Using reference material, developers will relate the differences between Python versions | B |
| | s. Developers will use Python in the Command Line/Terminal | 3c |
| | w. Developers will use Python to interact with native file systems (e.g., open, read, write, close) | 3c |
| | | |
| 3. | Networks Concepts - Developers will gain understanding of Network configurations and Networking fundamentals. They will apply their knowledge of Network concepts to perform socket programming.<sup>7</sup> |
| | j. Developers will demonstrate the ability to implement a Python socket-based TCP client | 3c |
| | k. Developers will demonstrate the ability to implement a Python socket-based UDP client | 3c |
| | l. Developers will demonstrate the ability to implement a Python socket-based TCP server | 3c |
| | m. Developers will demonstrate the ability to implement a Python socket-based UDP server | 3c |
| | n. Developers will programmatically access web pages via Python | 3c |
| | o. Developers will discuss String-based protocols | B |
| | p. Developers will pass data between client-servers using Protobuf as a serialization technology | 3c |
| | | |
| 4. | Assembly - Developers will demonstrate their understanding & coding capability in the use of Assembly programming<sup>8</sup> |
| | **No changes** | | | 
| | | |


# V. Supplemental Technical Skills

|Task| Sub-task & Description | Apprentice Proficiency Code |
|----|---------------------|-------------------|
| 1. | GDB - Developers will demonstrate their ability to debug their programs using GDB | |
| | **No changes** | |
| | | |
| 2. | GCC - Developers will demonstrate their ability to build their programs using GCC | |
| | **No changes** | |
| 3. | Git - Developers will demonstrate their understanding and ability to use a Version Control System ||
| | **No changes** | |
| | | | 

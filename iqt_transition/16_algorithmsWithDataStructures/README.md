# Section 16: Algorithms with Data Structures (Part 1)

### Description:

In this section, we will be going over algorithms that act on data structures. There are many... many algorithms out in the wild. We will just be going over a few of the basic ones this section. We will also be going over some slightly more advanced ones next section. Feel free to look up and experiment with additional algorithms if time permits. 

### Objectives

* Understand how the following algorithms work and be able to utilize each one to act on data structures:
    * Linear Search
    * Binary Search
    * Insertion Sort
    * Selection Sort
    * Bubble Sort
    * Merge Sort
    * Heap Sort
    * Quick Sort
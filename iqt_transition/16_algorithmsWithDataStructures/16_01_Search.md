# Section 16.01: Linear and Binary Search

## Linear Search

* **Linear Search** simply searches each element of an array, one by one, from the leftmost element and compares `x` with each element of the array. 
* We can implement this a few ways, a common way to do so is:
    * Do as above, if x matches with an element, return the index
    * If x doesn't match any of the elements, return -1

![](../assets/linear.png)
##### Source: https://cdncontribute.geeksforgeeks.org/wp-content/uploads/Linear.png

### Code Example

* A couple things will have to be known/provided:
    * The array to be searched `someArray[]`
    * The number of elements to be searched `n`
    * The key to search for `x`

```c
int linearSearch(int someArray[], int n, int x)
{
    for (int i = 0; i < n; i++)
    {
        if (someArray[i] == x)
            {
                return i;
            }
    }
    return -1;
}
```

### Practicality

Linear search has a realitivly high time complexity. Thus it's rarely used because other algorithms allow for significantly faster searching in comparision .

## Binary Search

* **Binary Search** searches a sorted array by repeatedly dividing the search interval in half. 
* We first start by checking the middle of the sorted array, if the value matches, we return it. 
    * If the value is lower than the key, we search the lower half of the array. 
    * If the value is higher than the key, we search the higher half of the array. 

![](../assets/binarySearch1.png)
##### Source: https://en.wikipedia.org/wiki/Binary_search_algorithm

### Code Example:

* A couple things will have to be known/provided:
    * The array to be searched `someArray[]`
    * The lowest element to check `l`
    * The highest element to check `h`
    * The key to search for `x`
* This could be done either recursivly or iteratively... we will go over iterative. You will attempt this search using a recursive method. 

```c
int binarySearch(int someArray[], int l, int h, int x)
{
    while (l <= r)
    {
        // obtain the middle of the array
        int mid = l + (h-1)/2;

        if (someArray[mid] == x)
        {
            return mid;
        }
        else if (someArray[mid] < x)
        {
            l = mid+1;
        }
        else
        {
            h = mid-1;
        }
    }
    return -1;
}

int main()
{
    int muhArray[] = {1,2,3,4,5,6,7,8,9,10}
    int n = sizeof(muhArray) / sizeof(muhArray[0]);
    int x = 3;
    int result = binarySearch(muhArray, 0, n-1, x);

    if (result == -1)
    {
        printf("Element is not present\n");
    }
    else
    {
        printf("Element is present at %d", result);
    }
    return 0;
}
```